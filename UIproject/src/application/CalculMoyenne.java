package application;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalculMoyenne extends JFrame implements ActionListener {
	private JLabel titreLabel, mat1, mat2, moy1, moy2, cof1, cof2, resultat;
	private JTextField tm1, tm2, tmoy1, tmoy2, tcof1, tcof2;
	private String titre_icone;
	private int width;
	private int height;
	private String titre;
	private Container container;
	private JButton button;
	private JPanel ligne1, ligne2, ligne3;

	public CalculMoyenne() {
		this.titre_icone = "Formation";
		this.width = 700;
		this.height = 300;
		this.setTitle(titre_icone);
		this.setSize(width, height);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		container = this.getContentPane();
		GridLayout gridlayout = new GridLayout(4, 1);
		container.setLayout(gridlayout);
		this.windows_titre();
		this.windows_corps();
		this.button.addActionListener(this);
		this.setVisible(true);
	}

	public void windows_titre() {
		this.titre = "Calcule des moyennes";
		titreLabel = new JLabel(titre);
		JPanel titrePanel = new JPanel();
		titrePanel.add(titreLabel);
		container.add(titrePanel);

	}

	public void windows_corps() {
		this.ligne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		this.mat1 = new JLabel("Matiere 1 : ");
		this.tm1 = new JTextField(25);
		this.moy1 = new JLabel("Moyenne 1 : ");
		tmoy1 = new JTextField(8);
		this.cof1 = new JLabel("coef : ");
		this.tcof1 = new JTextField(8);
		this.ligne1.add(this.mat1);
		this.ligne1.add(this.tm1);
		this.ligne1.add(this.moy1);
		this.ligne1.add(this.tmoy1);
		this.ligne1.add(this.cof1);
		this.ligne1.add(this.tcof1);
		this.container.add(this.ligne1);
		//////////////////////
		this.ligne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		this.mat2 = new JLabel("Matiere 2 : ");
		this.tm2 = new JTextField(25);
		this.moy2 = new JLabel("Moyenne 2 : ");
		this.tmoy2 = new JTextField(8);
		this.cof2 = new JLabel("coef : ");
		this.tcof2 = new JTextField(8);
		this.ligne2.add(this.mat2);
		this.ligne2.add(this.tm2);
		this.ligne2.add(this.moy2);
		this.ligne2.add(this.tmoy2);
		this.ligne2.add(this.cof2);
		this.ligne2.add(this.tcof2);
		this.container.add(this.ligne2);
		////////////////////////////
		this.ligne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		this.button = new JButton("Calcul Moyenne : ");
		this.resultat = new JLabel();
		this.ligne3.add(this.button);
		this.ligne3.add(this.resultat);
		this.container.add(this.ligne3);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == this.button) {
			double note1, note2, moyenne, noten;
			int co, co2, con;
			note1 = Double.valueOf(this.tmoy1.getText());
			note2 = Double.valueOf(this.tmoy2.getText());
			co = Integer.valueOf(this.tcof1.getText());
			co2 = Integer.valueOf(this.tcof2.getText());
			noten = note1 * co + note2 * co2;
			con = co + co2;
			moyenne = noten / con;
			this.resultat.setText(Double.toString(moyenne));
		} else {
			this.resultat.setText("null");
		}

	}
}
