package calculatrice;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Calculatrice extends JFrame implements ActionListener {

	private Container container;
	private JTextField input;
	private JPanel scan;
	private JButton b0;
	private JButton b1;
	private JButton b2;
	private JButton b3;
	private JButton b4;
	private JButton b5;
	private JButton b6;
	private JButton b7;
	private JButton b8;
	private JButton b9;
	private JPanel lineOne;
	private JPanel lineTwo;
	private JPanel lineThree;
	private JPanel lineFour;
	private JPanel lines;
	private JPanel lineOp;
	private JButton bDiv;
	private JButton bMul;
	private JButton bSub;
	private JButton bAdd;
	private JButton bEqual;
	private JPanel lineOperations;
	private JPanel lineOp2;
	private JPanel lineOp3;
	private JPanel lineOp4;
	private JPanel lineDiv;
	private JPanel lineMul;

	public Calculatrice() {
		this.setTitle("Calculatrice ");
		this.setSize(300,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		this.container = this.getContentPane();
		
		this.afficher();
		this.b0.addActionListener(this);
		this.b1.addActionListener(this);
		this.b2.addActionListener(this);
		this.b3.addActionListener(this);
		this.b4.addActionListener(this);
		this.b5.addActionListener(this);
		this.b6.addActionListener(this);
		this.b7.addActionListener(this);
		this.b8.addActionListener(this);
		this.b9.addActionListener(this);
		this.bDiv.addActionListener(this);
		this.bMul.addActionListener(this);
		this.bSub.addActionListener(this);
		this.bAdd.addActionListener(this);
		this.bEqual.addActionListener(this);
		this.setVisible(true);
	}
public void afficher() {
	this.input=new JTextField(25);
	this.scan=new JPanel();
	this.scan.add(this.input);
	this.scan.setBackground(Color.gray);
	this.container.add(this.scan,BorderLayout.NORTH);
	this.b0=new JButton("0");
	//this.b0.setMinimumSize(new Dimension(70,60));
	this.b0.setPreferredSize(new Dimension(70,50));
	this.b1=new JButton("1");
	this.b1.setPreferredSize(new Dimension(70,50));
	this.b2=new JButton("2");
	this.b2.setPreferredSize(new Dimension(70,50));
	this.b3=new JButton("3");
	this.b3.setPreferredSize(new Dimension(70,50));
	this.b4=new JButton("4");
	this.b4.setPreferredSize(new Dimension(70,50));
	this.b5=new JButton("5");
	this.b5.setPreferredSize(new Dimension(70,50));
	this.b6=new JButton("6");
	this.b6.setPreferredSize(new Dimension(70,50));
	this.b7=new JButton("7");
	this.b7.setPreferredSize(new Dimension(70,50));
	this.b8=new JButton("8");
	this.b8.setPreferredSize(new Dimension(70,50));
	this.b9=new JButton("9");
	this.b9.setPreferredSize(new Dimension(70,50));
	this.lineOne=new JPanel();
	//this.lineOne.setSize(70,60);
	this.lineOne.add(b1);
	this.lineOne.add(b2);
	this.lineOne.add(b3);
	this.lineTwo=new JPanel();
	//this.lineTwo.setSize(70,60);
	this.lineTwo.add(b4);
	this.lineTwo.add(b5);
	this.lineTwo.add(b6);
	this.lineThree=new JPanel();
	//this.lineThree.setSize(70,60);
	this.lineThree.add(b7);
	this.lineThree.add(b8);
	this.lineThree.add(b9);
	this.lineFour=new JPanel();
	//this.lineFour.setSize(70,60);
	this.lineFour.add(b0);
	this.lines=new JPanel();
	this.lines.setLayout(new BoxLayout(this.lines,BoxLayout.Y_AXIS));
	this.lines.setSize(70,60);
	this.lines.add(this.lineOne);
	this.lines.add(this.lineTwo);
	this.lines.add(this.lineThree);
	this.lines.add(this.lineFour);
	this.container.add(lines,BorderLayout.WEST);
	//this.b0=new JButton("");
	this.lineOp=new JPanel();
	this.lineOp.setLayout(new GridLayout(2,1));
	this.bDiv=new JButton("/");
	this.lineDiv=new JPanel(new FlowLayout());
	this.lineDiv.add(this.bDiv);
	this.bDiv.setPreferredSize(new Dimension(50,40));
	this.bMul=new JButton("*");
	this.lineMul=new JPanel(new FlowLayout());
	this.lineMul.add(this.bMul);
	this.bMul.setPreferredSize(new Dimension(50,40));
	this.bSub=new JButton("-");
	this.bSub.setPreferredSize(new Dimension(50,40));
	this.bAdd=new JButton("+");
	this.bAdd.setPreferredSize(new Dimension(50,40));
	this.bEqual=new JButton("=");
	this.bEqual.setPreferredSize(new Dimension(50,40));
	this.lineOp.add(this.lineDiv);
	this.lineOp.add(this.lineMul);
	this.lineOperations=new JPanel();
	this.lineOperations.setLayout(new BoxLayout(this.lineOperations,BoxLayout.Y_AXIS));
	
	this.lineOp2=new JPanel();
	this.lineOp2.add(this.bSub);
	this.lineOp3=new JPanel();
	this.lineOp3.add(this.bAdd);
	this.lineOp4=new JPanel();
	this.lineOp4.add(this.bEqual);
	
	this.lineOperations.add(this.lineOp);
	this.lineOperations.add(this.lineOp2);
	this.lineOperations.add(this.lineOp3);
	this.lineOperations.add(this.lineOp4);
	
	this.container.add(this.lineOperations,BorderLayout.CENTER);
}
@Override
public void actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	if(e.getSource()==this.b1) {
		this.input.setText(this.input.getText()+"1");
	}
	
	else if(e.getSource()==this.b2) {
		this.input.setText(this.input.getText()+"2");
	}
	
	else if(e.getSource()==this.b3) {
		this.input.setText(this.input.getText()+"3");
	}
	
	else if(e.getSource()==this.b4) {
		this.input.setText(this.input.getText()+"4");
	}
	
	else if(e.getSource()==this.b5) {
		this.input.setText(this.input.getText()+"5");
	}
	
	else if(e.getSource()==this.b6) {
		this.input.setText(this.input.getText()+"6");
	}
	
	else if(e.getSource()==this.b7) {
		this.input.setText(this.input.getText()+"7");
	}
	
	else if(e.getSource()==this.b8) {
		this.input.setText(this.input.getText()+"8");
	}
	
	else if(e.getSource()==this.b9) {
		this.input.setText(this.input.getText()+"9");
	}
	
	else if(e.getSource()==this.b0) {
		this.input.setText(this.input.getText()+"0");
	}
	
	else if(e.getSource()==this.bDiv) {
		this.input.setText(this.input.getText()+"/");
	}
	
	else if(e.getSource()==this.bMul) {
		this.input.setText(this.input.getText()+"*");
	}
	
	else if(e.getSource()==this.bSub) {
		this.input.setText(this.input.getText()+"-");
	}
	
	else if(e.getSource()==this.bAdd) {
		this.input.setText(this.input.getText()+"+");
	}
	
	else {
		try {
		int lg=this.input.getText().length();
		int i=0;
		String chaine=this.input.getText();
		//this.input.setText(Integer.toString(lg));
		char[]t=new char[lg];
		t=chaine.toCharArray();
		//List<String>ls=new ArrayList<String>();
		String [] ls=new String[3];
		String opp = "";
		char os=' ';
		ls[0]="";
		ls[1]="";
		ls[2]="";
		int k=0;
		while(i<lg) {
			if(t[i]=='0') {
				ls[k]+=t[i];
			}
			else if(t[i]=='1') {ls[k]+=t[i];}
			else if(t[i]=='2') {ls[k]+=t[i];}
			else if(t[i]=='3') {ls[k]+=t[i];}
			else if(t[i]=='4') {ls[k]+=t[i];}
			else if(t[i]=='5') {ls[k]+=t[i];}
			else if(t[i]=='6') {ls[k]+=t[i];}
			else if(t[i]=='7') {ls[k]+=t[i];}
			else if(t[i]=='8') {ls[k]+=t[i];}
			else if(t[i]=='9') {ls[k]+=t[i];}
			else if(t[i]=='.') {ls[k]+=t[i];}
			else {
				k++;
				os=t[i];
				//System.out.println(t[i]+"\n"+ls[k]);
				
				
				
			}
	
			i++;	
		}
		
			//for(int j=0;j<ls.length;j++) {
			//System.out.println(ls[j]);}
		double op1,op2,res;
		res=0;
		op1=Double.parseDouble(ls[0]);
		op2=Double.parseDouble(ls[1]);
		if(os=='/') {
			res=op1/op2; 
		}
		else if(os=='*') {
			res=op1*op2;
		}
		else if(os=='+') {
			res=op1+op2;
		}
		else if(os=='-'){
			res=op2-op2;
		}
		else {
			System.out.println("Erreur");
		}
		//System.out.println(res);
		this.input.setText(Double.toString(res));
		}
	catch(Exception o) {
		this.input.setText("");}
	
}
}
}
