package mise_en_forme;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class FrmAffichage extends JFrame implements ActionListener {
	private JLabel saisie, affichage;
	private JTextField tSaisie, tAffichage;
	private JRadioButton choixCouleur1, choixcouleur2;
	private JCheckBox choix;
	private String couleur1, couleur2;
	private JPanel ligne1, ligne2v1, ligne2v2, ligne2, ligne3;
	private Container container;
	private ButtonGroup bouttons;

	public FrmAffichage() {
		this.setTitle("Mise en forme d'un texte");
		this.setSize(400, 200);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.container = this.getContentPane();
		this.windowCorps();
		// this.container.actionPerformed(this);
		this.tAffichage.setText(this.tSaisie.getText());
		this.choixCouleur1.addActionListener(this);
		this.choixcouleur2.addActionListener(this);
		this.choix.addActionListener(this);
		this.container.setLayout(new GridLayout(3, 2));
		this.setVisible(true);
	}

	public void windowCorps() {

		this.saisie = new JLabel("Saisir Votre texte  ");
		this.tSaisie = new JTextField(25);
		this.bouttons = new ButtonGroup();
		this.choixCouleur1 = new JRadioButton("rouge", false);
		this.choixcouleur2 = new JRadioButton("bleu", true);
		this.bouttons.add(this.choixCouleur1);
		this.bouttons.add(this.choixcouleur2);
		this.choix = new JCheckBox("Majuscule", false);
		this.ligne2v1 = new JPanel(new GridLayout(2, 1));
		this.ligne2v1.add(choixCouleur1);
		this.ligne2v1.add(choixcouleur2);
		this.affichage = new JLabel("Saisir Votre texte  ");
		this.tAffichage = new JTextField(25);
		this.container.add(saisie);
		this.container.add(tSaisie);
		this.container.add(ligne2v1);
		this.container.add(choix);
		this.container.add(affichage);
		this.container.add(tAffichage);
		// this.container.add(ligne2);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		/*
		 * if(this.choix.isSelected()) {
		 * System.out.println(this.tSaisie.getText().toUpperCase()); }
		 */
		if ((this.choixCouleur1.getSelectedObjects() != null) && (this.choix.isSelected() == true)) {
			String t = this.tSaisie.getText();
			String text = t.toUpperCase();
			this.tAffichage.setText(text);
			this.tAffichage.setForeground(Color.red);
		} else if ((this.choixCouleur1.getSelectedObjects() != null) && (this.choix.isSelected() == false)) {
			String t = this.tSaisie.getText();
			this.tAffichage.setText(t);
			this.tAffichage.setForeground(Color.red);
		} else if ((this.choixcouleur2.getSelectedObjects() != null) && (this.choix.isSelected() == true)) {
			String t = this.tSaisie.getText();
			String text = t.toUpperCase();
			this.tAffichage.setText(text);
			this.tAffichage.setForeground(Color.blue);
		} else {
			String t = this.tSaisie.getText();
			System.out.println(t);
			this.tAffichage.setText(t);
			this.tAffichage.setForeground(Color.blue);
		}

	}
}
