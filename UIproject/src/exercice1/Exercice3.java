package exercice1;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Exercice3 {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Exerice3");
		frame.setSize(300, 200);
		JButton btn = new JButton("Bouton");
		JTextField txt = new JTextField();
		JLabel lbl = new JLabel("un petit texte");
		JCheckBox chk = new JCheckBox("Case � cocher");
		ButtonGroup bg = new ButtonGroup();
		JRadioButton rb1 = new JRadioButton("Choix1", true);
		JRadioButton rb2 = new JRadioButton("Choix2");
		bg.add(rb1);
		bg.add(rb2);
		JComboBox<String> cbo = new JComboBox<String>();
		cbo.addItem("TI");
		cbo.addItem("SEG");
		JPanel pane = new JPanel();
		pane.setLayout(new GridLayout(3, 2));
		pane.add(btn);
		pane.add(txt);
		pane.add(lbl);
		pane.add(chk);
		pane.add(rb1);
		pane.add(rb2);
		pane.add(cbo);
		frame.getContentPane().add(pane, BorderLayout.CENTER);
		frame.setVisible(true);
		/*
		 * L'objet JChekBox permet de cr�er une case � cocher L'objet JRadioButton
		 * permet de cr�er un bouton radio L'objet ButtonGroup permet de ranger les
		 * buttons radio L'objet JComboBox permet de cr�er une liste a un seul choix
		 * 
		 */
	}
}
